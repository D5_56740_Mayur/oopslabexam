package com.app.singer;

import java.util.ArrayList;
import java.util.List;

import custom_exception.SingerNotFoundException;
import custom_exception.overrated_exception;
import custom_exception.underrated_exception;

public interface processes {
	public abstract void addSinger(singer name,ArrayList<singer> singerList);
	public abstract void removeSinger(singer name,ArrayList<singer> singerList) throws SingerNotFoundException;
	public abstract void modifySingersRating(singer name,ArrayList<singer> singerList) throws underrated_exception,overrated_exception;
	public abstract singer searchSingerByName(singer name,ArrayList<singer> singerList) throws SingerNotFoundException;
	public abstract void sortSingerByName(singer name,ArrayList<singer> singerList);
	public abstract List<singer> showAllSingers();
	
	
	
	
	

}

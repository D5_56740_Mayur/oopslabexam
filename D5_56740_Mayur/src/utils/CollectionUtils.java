package utils;

import java.text.ParseException;
import java.util.ArrayList;

import com.app.singer.singer;

import custom_exception.SingerNotFoundException;

public class CollectionUtils {
	public static ArrayList<singer> populateSampleData() throws ParseException,SingerNotFoundException{
		ArrayList<singer> list=new ArrayList<singer>();
		
		//[name,gender,age,email_id,contact,rating]
		list.add(new singer("MAYUR","MALE",24,"abc@gmail.com",911,5));
		list.add(new singer("Akshay","MALE",22,"xyc@gmail.com",921,4));
		list.add(new singer("Hrushi","MALE",24,"adh@gmail.com",941,4));
		
		return list;
		
	}

}
